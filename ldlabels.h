#ifndef _RYUKOS_PLATFORM_LINKER_DEFS_H_
#define _RYUKOS_PLATFORM_LINKER_DEFS_H_

#ifdef __cplusplus
extern "C" {
#endif

    extern usize SECTION_START(text);
    
    extern usize SECTION_END(text);
    
    extern usize SECTION_SIZE(text);

    extern usize SECTION_START(data);
    
    extern usize SECTION_END(data);
    
    extern usize SECTION_SIZE(data);
    
    extern usize SECTION_LOAD(data);
    
    extern usize SECTION_START(bss);
    
    extern usize SECTION_END(bss);
    
    extern usize SECTION_SIZE(bss);
    
    extern usize SECTION_START(heap);
    
    extern usize SECTION_END(heap);
    
    extern usize SECTION_SIZE(heap);
    
    extern usize SECTION_END(stack);
    
#ifdef __cplusplus
}
#endif

#endif  //_RYUKOS_PLATFORM_LINKER_DEFS_H_
