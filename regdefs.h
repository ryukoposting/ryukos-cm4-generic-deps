

#ifndef _GENERIC_CM4_REGDEFS_H_
#define _GENERIC_CM4_REGDEFS_H_

#ifdef __cplusplus
  #define   __I     volatile             /*!< Defines 'read only' permissions */
#else
  #define   __I     volatile const       /*!< Defines 'read only' permissions */
#endif
#define     __O     volatile             /*!< Defines 'write only' permissions */
#define     __IO    volatile             /*!< Defines 'read / write' permissions */

/* following defines should be used for structure members */
#define     __IM     volatile const      /*! Defines 'read only' structure member permissions */
#define     __OM     volatile            /*! Defines 'write only' structure member permissions */
#define     __IOM    volatile            /*! Defines 'read / write' structure member permissions */

#ifdef __cplusplus
extern "C" {
#endif

#define WR_REG(reg, mask, pos, val) reg = (((u32)val << pos) & \
                                            ((u32)mask)) | \
                                          (reg & ((u32)~((u32)mask)))

/* Macro to modify desired bitfields of a register */
#define WR_REG_SIZE(reg, mask, pos, val, size) {  \
u##size## VAL1 = (u##size##)((u##size##)val << pos); \
u##size## VAL2 = (u##size##) (VAL1 & (u##size##)mask); \
u##size## VAL3 = (u##size##)~((u##size##)mask); \
u##size## VAL4 = (u##size##) ((u##size##)reg & VAL3); \
reg = (u##size##_t) (VAL2 | VAL4);\
}

/** Macro to read bitfields from a register */
#define RD_REG(reg, mask, pos) (((u32)reg & (u32)mask) >> pos)

/** Macro to read bitfields from a register */
#define RD_REG_SIZE(reg, mask, pos,size) ((u##size##)(((u32)reg & \
                                                      (u32)mask) >> pos) )

/** Macro to set a bit in register */
#define SET_BIT(reg, pos)     (reg |= ((u32)1<<pos))

/** Macro to clear a bit in register */
#define CLR_BIT(reg, pos)     (reg = reg & (u32)(~((u32)1<<pos)) )

typedef enum {
  reset_handler_IRQn            = -15,
  NonMaskableInt_IRQn           = -14,
  HardFault_IRQn                = -13,
  MemoryManagement_IRQn         = -12,
  BusFault_IRQn                 = -11,
  UsageFault_IRQn               = -10,
  SVCall_IRQn                   =  -5,
  DebugMonitor_IRQn             =  -4,
  PendSV_IRQn                   =  -2,
  systick_handler_IRQn          =  -1,
} IRQn_Type;

typedef struct {                               /*!< (@ 0xE000E000) PPB Structure                                          */
  __I  u32  RESERVED[2];
  __IO u32  ACTLR;                             /*!< (@ 0xE000E008) Auxiliary Control Register                             */
  __I  u32  RESERVED1;
  __IO u32  SYST_CSR;                          /*!< (@ 0xE000E010) SysTick Control and Status Register                    */
  __IO u32  SYST_RVR;                          /*!< (@ 0xE000E014) SysTick Reload Value Register                          */
  __IO u32  SYST_CVR;                          /*!< (@ 0xE000E018) SysTick Current Value Register                         */
  __IO u32  SYST_CALIB;                        /*!< (@ 0xE000E01C) SysTick Calibration Value Register r                   */
  __I  u32  RESERVED2[56];
  __IO u32  NVIC_ISER0;                        /*!< (@ 0xE000E100) Interrupt Set-enable Register 0                        */
  __IO u32  NVIC_ISER1;                        /*!< (@ 0xE000E104) Interrupt Set-enable Register 1                        */
  __IO u32  NVIC_ISER2;                        /*!< (@ 0xE000E108) Interrupt Set-enable Register 2                        */
  __IO u32  NVIC_ISER3;                        /*!< (@ 0xE000E10C) Interrupt Set-enable Register 3                        */
  __I  u32  RESERVED3[28];
  __IO u32  NVIC_ICER0;                        /*!< (@ 0xE000E180) Interrupt Clear-enable Register 0                      */
  __IO u32  NVIC_ICER1;                        /*!< (@ 0xE000E184) Interrupt Clear-enable Register 1                      */
  __IO u32  NVIC_ICER2;                        /*!< (@ 0xE000E188) Interrupt Clear-enable Register 2                      */
  __IO u32  NVIC_ICER3;                        /*!< (@ 0xE000E18C) Interrupt Clear-enable Register 3                      */
  __I  u32  RESERVED4[28];
  __IO u32  NVIC_ISPR0;                        /*!< (@ 0xE000E200) Interrupt Set-pending Register 0                       */
  __IO u32  NVIC_ISPR1;                        /*!< (@ 0xE000E204) Interrupt Set-pending Register 1                       */
  __IO u32  NVIC_ISPR2;                        /*!< (@ 0xE000E208) Interrupt Set-pending Register 2                       */
  __IO u32  NVIC_ISPR3;                        /*!< (@ 0xE000E20C) Interrupt Set-pending Register 3                       */
  __I  u32  RESERVED5[28];
  __IO u32  NVIC_ICPR0;                        /*!< (@ 0xE000E280) Interrupt Clear-pending Register 0                     */
  __IO u32  NVIC_ICPR1;                        /*!< (@ 0xE000E284) Interrupt Clear-pending Register 1                     */
  __IO u32  NVIC_ICPR2;                        /*!< (@ 0xE000E288) Interrupt Clear-pending Register 2                     */
  __IO u32  NVIC_ICPR3;                        /*!< (@ 0xE000E28C) Interrupt Clear-pending Register 3                     */
  __I  u32  RESERVED6[28];
  __IO u32  NVIC_IABR0;                        /*!< (@ 0xE000E300) Interrupt Active Bit Register 0                        */
  __IO u32  NVIC_IABR1;                        /*!< (@ 0xE000E304) Interrupt Active Bit Register 1                        */
  __IO u32  NVIC_IABR2;                        /*!< (@ 0xE000E308) Interrupt Active Bit Register 2                        */
  __IO u32  NVIC_IABR3;                        /*!< (@ 0xE000E30C) Interrupt Active Bit Register 3                        */
  __I  u32  RESERVED7[60];
  __IO u32  NVIC_IPR0;                         /*!< (@ 0xE000E400) Interrupt Priority Register 0                          */
  __IO u32  NVIC_IPR1;                         /*!< (@ 0xE000E404) Interrupt Priority Register 1                          */
  __IO u32  NVIC_IPR2;                         /*!< (@ 0xE000E408) Interrupt Priority Register 2                          */
  __IO u32  NVIC_IPR3;                         /*!< (@ 0xE000E40C) Interrupt Priority Register 3                          */
  __IO u32  NVIC_IPR4;                         /*!< (@ 0xE000E410) Interrupt Priority Register 4                          */
  __IO u32  NVIC_IPR5;                         /*!< (@ 0xE000E414) Interrupt Priority Register 5                          */
  __IO u32  NVIC_IPR6;                         /*!< (@ 0xE000E418) Interrupt Priority Register 6                          */
  __IO u32  NVIC_IPR7;                         /*!< (@ 0xE000E41C) Interrupt Priority Register 7                          */
  __IO u32  NVIC_IPR8;                         /*!< (@ 0xE000E420) Interrupt Priority Register 8                          */
  __IO u32  NVIC_IPR9;                         /*!< (@ 0xE000E424) Interrupt Priority Register 9                          */
  __IO u32  NVIC_IPR10;                        /*!< (@ 0xE000E428) Interrupt Priority Register 10                         */
  __IO u32  NVIC_IPR11;                        /*!< (@ 0xE000E42C) Interrupt Priority Register 11                         */
  __IO u32  NVIC_IPR12;                        /*!< (@ 0xE000E430) Interrupt Priority Register 12                         */
  __IO u32  NVIC_IPR13;                        /*!< (@ 0xE000E434) Interrupt Priority Register 13                         */
  __IO u32  NVIC_IPR14;                        /*!< (@ 0xE000E438) Interrupt Priority Register 14                         */
  __IO u32  NVIC_IPR15;                        /*!< (@ 0xE000E43C) Interrupt Priority Register 15                         */
  __IO u32  NVIC_IPR16;                        /*!< (@ 0xE000E440) Interrupt Priority Register 16                         */
  __IO u32  NVIC_IPR17;                        /*!< (@ 0xE000E444) Interrupt Priority Register 17                         */
  __IO u32  NVIC_IPR18;                        /*!< (@ 0xE000E448) Interrupt Priority Register 18                         */
  __IO u32  NVIC_IPR19;                        /*!< (@ 0xE000E44C) Interrupt Priority Register 19                         */
  __IO u32  NVIC_IPR20;                        /*!< (@ 0xE000E450) Interrupt Priority Register 20                         */
  __IO u32  NVIC_IPR21;                        /*!< (@ 0xE000E454) Interrupt Priority Register 21                         */
  __IO u32  NVIC_IPR22;                        /*!< (@ 0xE000E458) Interrupt Priority Register 22                         */
  __IO u32  NVIC_IPR23;                        /*!< (@ 0xE000E45C) Interrupt Priority Register 23                         */
  __IO u32  NVIC_IPR24;                        /*!< (@ 0xE000E460) Interrupt Priority Register 24                         */
  __IO u32  NVIC_IPR25;                        /*!< (@ 0xE000E464) Interrupt Priority Register 25                         */
  __IO u32  NVIC_IPR26;                        /*!< (@ 0xE000E468) Interrupt Priority Register 26                         */
  __IO u32  NVIC_IPR27;                        /*!< (@ 0xE000E46C) Interrupt Priority Register 27                         */
  __I  u32  RESERVED8[548];
  __I  u32  CPUID;                             /*!< (@ 0xE000ED00) CPUID Base Register                                    */
  __IO u32  ICSR;                              /*!< (@ 0xE000ED04) Interrupt Control and State Register                   */
  __IO u32  VTOR;                              /*!< (@ 0xE000ED08) Vector Table Offset Register                           */
  __IO u32  AIRCR;                             /*!< (@ 0xE000ED0C) Application Interrupt and Reset Control Register       */
  __IO u32  SCR;                               /*!< (@ 0xE000ED10) System Control Register                                */
  __IO u32  CCR;                               /*!< (@ 0xE000ED14) Configuration and Control Register                     */
  __IO u32  SHPR1;                             /*!< (@ 0xE000ED18) System Handler Priority Register 1                     */
  __IO u32  SHPR2;                             /*!< (@ 0xE000ED1C) System Handler Priority Register 2                     */
  __IO u32  SHPR3;                             /*!< (@ 0xE000ED20) System Handler Priority Register 3                     */
  __IO u32  SHCSR;                             /*!< (@ 0xE000ED24) System Handler Control and State Register              */
  __IO u32  CFSR;                              /*!< (@ 0xE000ED28) Configurable Fault Status Register                     */
  __IO u32  HFSR;                              /*!< (@ 0xE000ED2C) HardFault Status Register                              */
  __I  u32  RESERVED9;
  __IO u32  MMFAR;                             /*!< (@ 0xE000ED34) MemManage Fault Address Register                       */
  __IO u32  BFAR;                              /*!< (@ 0xE000ED38) BusFault Address Register                              */
  __IO u32  AFSR;                              /*!< (@ 0xE000ED3C) Auxiliary Fault Status Register                        */
  __I  u32  RESERVED10[18];
  __IO u32  CPACR;                             /*!< (@ 0xE000ED88) Coprocessor Access Control Register                    */
  __I  u32  RESERVED11;
  __I  u32  MPU_TYPE;                          /*!< (@ 0xE000ED90) MPU Type Register                                      */
  __IO u32  MPU_CTRL;                          /*!< (@ 0xE000ED94) MPU Control Register                                   */
  __IO u32  MPU_RNR;                           /*!< (@ 0xE000ED98) MPU Region Number Register                             */
  __IO u32  MPU_RBAR;                          /*!< (@ 0xE000ED9C) MPU Region Base Address Register                       */
  __IO u32  MPU_RASR;                          /*!< (@ 0xE000EDA0) MPU Region Attribute and Size Register                 */
  __IO u32  MPU_RBAR_A1;                       /*!< (@ 0xE000EDA4) MPU Region Base Address Register A1                    */
  __IO u32  MPU_RASR_A1;                       /*!< (@ 0xE000EDA8) MPU Region Attribute and Size Register A1              */
  __IO u32  MPU_RBAR_A2;                       /*!< (@ 0xE000EDAC) MPU Region Base Address Register A2                    */
  __IO u32  MPU_RASR_A2;                       /*!< (@ 0xE000EDB0) MPU Region Attribute and Size Register A2              */
  __IO u32  MPU_RBAR_A3;                       /*!< (@ 0xE000EDB4) MPU Region Base Address Register A3                    */
  __IO u32  MPU_RASR_A3;                       /*!< (@ 0xE000EDB8) MPU Region Attribute and Size Register A3              */
  __I  u32  RESERVED12[81];
  __O  u32  STIR;                              /*!< (@ 0xE000EF00) Software Trigger Interrupt Register                    */
  __I  u32  RESERVED13[12];
  __IO u32  FPCCR;                             /*!< (@ 0xE000EF34) Floating-point Context Control Register                */
  __IO u32  FPCAR;                             /*!< (@ 0xE000EF38) Floating-point Context Address Register                */
  __IO u32  FPDSCR;                            /*!< (@ 0xE000EF3C) Floating-point Default Status Control Register         */
} PPB_Type;

#define GET_IRQ_HANDLER(N) IRQ_Hdlr_##N

#define PPB_BASE                        0xE000E000UL

    
#ifdef __cplusplus
}
#endif

#endif
