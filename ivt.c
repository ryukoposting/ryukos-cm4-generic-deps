#include <ryukos.h>
#include "ivt.h"

void default_handler(void)
{
    assert(0, "Default handler entered.");
}

void nmi_handler(void) __attribute__((weak, alias("default_handler")));
void hardfault_handler(void) __attribute__((weak, alias("default_handler")));
void memmanage_handler(void) __attribute__((weak, alias("default_handler")));
void busfault_handler(void) __attribute__((weak, alias("default_handler")));
void usagefault_handler(void) __attribute__((weak, alias("default_handler")));
// extern void __attribute__((naked)) pendsv_handler(void);

__attribute((section(".isr_vector")))
u32 *isr_vectors[] = {
    (u32 *) &_estack,
    (u32 *) reset_handler,
    (u32 *) nmi_handler,
    (u32 *) hardfault_handler,
    (u32 *) memmanage_handler,
    (u32 *) busfault_handler,
    (u32 *) usagefault_handler,
    0,
    0,
    0,
    0,
    (u32 *) svc_handler,
    0,
    0,
    (u32 *) pendsv_handler,
    (u32 *) systick_handler
};
