
#ifndef _RYUKOS_PLATFORM_IVT_H_
#define _RYUKOS_PLATFORM_IVT_H_

#include <sys/interrupts/interrupts.h>

#ifdef __cplusplus
extern "C" {
#endif

    void nmi_handler(void);
    void hardfault_handler(void);
    void memmanage_handler(void);
    void busfault_handler(void);
    void usagefault_handler(void);

#ifdef __cplusplus
}
#endif

#endif
