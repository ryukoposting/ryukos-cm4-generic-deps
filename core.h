#ifndef _CM4_CORE_HAL_H_
#define _CM4_CORE_HAL_H_

#include <ryukos.h>

#define RYUKOS_PLATFORMSPEC_GENERIC_ARM_CM4

/* General control registers */
#define CORE_CM4_SCR (*((u32 *)0xE000ED10))
#define CORE_CM4_CCR (*((u32 *)0xE000ED14))

/* Fault management/fault recovery register */
#define CORE_CM4_CFSR (*((u32 *)0xE000ED28))

/* Interrupt/exception control registers */
#define CORE_CM4_ICSR (*((u32 *)0xE000ED04))
#define CORE_CM4_AIRCR (*((u32 *)0xE000ED0C))
#define CORE_CM4_SHPR1 (*((u32 *)0xE000ED18))
#define CORE_CM4_SHPR2 (*((u32 *)0xE000ED1C))
#define CORE_CM4_SHPR3 (*((u32 *)0xE000ED20))
#define CORE_CM4_SHCSR (*((u32 *)0xE000ED24))

/* System tick control registers */
#define CORE_CM4_SYSTICK_CSR (*((u32 *)0xE000E010))
#define CORE_CM4_SYSTICK_RVR (*((u32 *)0xE000E014))
#define CORE_CM4_SYSTICK_CVR (*((u32 *)0xE000E018))
#define CORE_CM4_SYSTICK_CALIB (*((u32 *)0xE000E01C))

#ifdef __cplusplus
extern "C" {
#endif

    typedef enum {
        FAULT_NONE = 0,
        FAULT_INSTRUCTION_ACCESS = 0x00000001,
        FAULT_DATA_ACCESS = 0x00000002,
        FAULT_MEMMANAGE_ON_UNSTACK_FROM_EXCEPTION = 0x00000008,
        FAULT_MEMMANAGE_ON_STACK_FOR_EXCEPTION = 0x00000010,
        FAULT_MEMMANAGE_FLOAT_LAZYSTATE = 0x00000020,
        FAULT_MEMMANAGE_ADDRESS_REGISTER = 0x00000080,
        FAULT_BUS_INSTRUCTION = 0x00000100,
        FAULT_BUS_PRECISE = 0x00000200,
        FAULT_BUS_IMPRECISE = 0x00000400,
        FAULT_BUS_ON_UNSTACK_FROM_EXCEPTION = 0x00000800,
        FAULT_BUS_ON_STACK_FOR_EXCEPTION = 0x00001000,
        FAULT_BUS_FLOAT_LAZYSTATE = 0x00002000,
        FAULT_BUS_ADDRESS_REGISTER = 0x00008000,
        FAULT_USAGE_UNDEFINED_INSTRUCTION = 0x00010000,
        FAULT_USAGE_INVALID_STATE = 0x00020000,
        FAULT_USAGE_INVALID_PC_LOAD = 0x00040000,
        FAULT_USAGE_NO_COPROC = 0x00080000,
        FAULT_USAGE_UNALIGNED = 0x01000000,
        FAULT_USAGE_DIVBYZERO = 0x02000000,
    } corefault_t;
    
    static inline void core_configureexceptions()
    {
        CORE_CM4_AIRCR = (CORE_CM4_AIRCR & 0xFFFFF8FF) | (0b001 << 8);
        CORE_CM4_SHPR3 = (CORE_CM4_SHPR3 & 0x0000FFFF) | (0x04050000);
    }
    
    static inline void core_starttick()
    {
        CORE_CM4_SYSTICK_CSR = 0x00000000;

        CORE_CM4_SYSTICK_CVR = 0;

        if (((CORE_CM4_SYSTICK_CALIB & 0x80000000) == 0)
            && ((CORE_CM4_SYSTICK_CALIB & (0x00FFFFFF)) != 0)) {
            CORE_CM4_SYSTICK_RVR = CORE_CM4_SYSTICK_CALIB & (0x00FFFFFF);
            
        } else {
            CORE_CM4_SYSTICK_RVR = RYUKOS_PLATFORM_TICK_GUESS;
        }
        
        CORE_CM4_SYSTICK_CVR = 0;
        
        CORE_CM4_SYSTICK_CSR = 0x00000007;
    }
    
    static inline void core_resettick()
    {
        CORE_CM4_SYSTICK_CVR = 0;
    }

    static inline u8 core_systickpending()
    {
        return (CORE_CM4_ICSR & (0b1 << 26)) > 0;
    }
    
    static inline u8 core_systickisrenabled()
    {
        return (CORE_CM4_SYSTICK_CSR & (0b1 << 1)) > 0;
    }
    
    static inline corefault_t core_getfault()
    {
        return (corefault_t)CORE_CM4_CFSR;
    }
    
#ifdef __cplusplus
}
#endif

#endif
